library ieee;
use ieee.std_logic_1164.all;

entity controller is
    port(
        clk        : in  std_logic;
        reset_n    : in  std_logic;
        -- instruction opcode
        op         : in  std_logic_vector(5 downto 0);
        opx        : in  std_logic_vector(5 downto 0);
        -- activates branch condition
        branch_op  : out std_logic;
        -- immediate value sign extention
        imm_signed : out std_logic;
        -- instruction register enable
        ir_en      : out std_logic;
        -- PC control signals
        pc_add_imm : out std_logic;
        pc_en      : out std_logic;
        pc_sel_a   : out std_logic;
        pc_sel_imm : out std_logic;
        -- register file enable
        rf_wren    : out std_logic;
        -- multiplexers selections
        sel_addr   : out std_logic;
        sel_b      : out std_logic;
        sel_mem    : out std_logic;
        sel_pc     : out std_logic;
        sel_ra     : out std_logic;
        sel_rC     : out std_logic;
        -- write memory output
        read       : out std_logic;
        write      : out std_logic;
        -- alu op
        op_alu     : out std_logic_vector(5 downto 0)
    );
end controller;

architecture synth of controller is
TYPE State_type IS (Fetch1, Fetch2, Decode, I_OP, R_OP, Load_1, Load_2, Store, Branch, Call, Jump, unsignedI, immR, Break);

SIGNAL s_code_alu : std_logic_vector(5 downto 0);
SIGNAL s_next_state : State_type;
SIGNAL s_current_state : State_type;

begin


code_selector : process(op, opx) is
begin

CASE op IS
	WHEN "111010" => 

							if opx = "110001" then
								s_code_alu <= "000" & opx(5 downto 3);
							else if opx = "111001" then
								s_code_alu <= "001" & opx(5 downto 3);
							else if opx = "001000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "010000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "000110" then
								s_code_alu <= "100" & opx(5 downto 3);
							else if opx = "001110" then
								s_code_alu <= "100" & opx(5 downto 3);
							else if opx = "010110" then
								s_code_alu <= "100" & opx(5 downto 3);
							else if opx = "011110" then
								s_code_alu <= "100" & opx(5 downto 3);
							else if opx = "010011" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "011011" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "111011" then
								s_code_alu <= "110" & opx(5 downto 3);


							else if opx = "010010" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "011010" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "111010" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "011000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "100000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "101000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "110000" then
								s_code_alu <= "011" & opx(5 downto 3);
							else if opx = "000011" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "001011" then
								s_code_alu <= "110" & opx(5 downto 3);
							else if opx = "000010" then
								s_code_alu <= "110" & opx(5 downto 3);

							end if;
							
	WHEN "000100" =>
							s_code_alu <= "000000";
	
	WHEN "010111" => 
							s_code_alu <= "000000";
	
	WHEN "010101" =>  
							s_code_alu <= "000000";


	WHEN "001100" =>
							s_code_alu <= "100" & opx(5 downto 3);
	WHEN "010100" =>
							s_code_alu <= "100" & opx(5 downto 3);
	WHEN "011100" =>
							s_code_alu <= "100" & opx(5 downto 3);
	WHEN "001000" =>
							s_code_alu <= "011" & opx(5 downto 3);
	WHEN "010000" =>
							s_code_alu <= "011" & opx(5 downto 3);
	WHEN "011000" =>
							s_code_alu <= "011" & opx(5 downto 3);
	WHEN "100000" =>
							s_code_alu <= "011" & opx(5 downto 3);
	WHEN "101000" =>
							s_code_alu <= "011" & opx(5 downto 3);
	WHEN "110000" =>
							s_code_alu <= "011" & opx(5 downto 3);





-- Branch
	WHEN "000110" =>
							s_code_alu <= "100110";
	
	WHEN "001110" => 
							s_code_alu <= "001110";
	
	WHEN "010110" =>  
							s_code_alu <= "010110";

	WHEN "011110" =>	
							s_code_alu <= "011110";

	WHEN "100110" =>
							s_code_alu <= "100110";
	
	WHEN "101110" => 
							s_code_alu <= "101110";
	
	WHEN "110110" =>  
							s_code_alu <= "110110";
	
	WHEN OTHERS =>
							s_code_alu <= "000000";
							
end CASE;
end process;




State_Machine : process(s_current_state, op, opx) is
begin
CASE s_current_state IS
				WHEN Fetch1 => 
									s_next_state <= Fetch2;
									
				WHEN Fetch2 => 
									s_next_state <= Decode;
									
				WHEN Decode => 
									CASE op IS
										WHEN "111010" =>
																if (opx = "110001") or (opx = "111001") or (opx = "001000") or (opx = "010000") or (opx = "000110") or (opx = "001110") or (opx = "010110") or (opx = "011110") or (opx = "010011") or (opx = "011011") or (opx = "111011") or (opx = "011000") or (opx = "100000") or (opx = "101000") or (opx = "110000") or (opx = "000011") or (opx = "001011") then
																	s_next_state <= R_OP;
																else if opx = "011101" then
																	s_next_state <= Call;
																else if opx = "001101" || opx = "000101" then
																	s_next_state <= Jump;
																else if opx = "010010" || opx = "011010" || opx = " 111010" || opx = "000010" then
																	s_next_state <= immR
																else if opx = "110100" then
																	s_next_state <= Break;
																end if;
										WHEN "000100" =>
																s_next_state <= I_OP;
										WHEN "001000" =>
																s_next_state <= I_OP;
										WHEN "010000" =>
																s_next_state <= I_OP;
										WHEN "011000" =>
																s_next_state <= I_OP;
										WHEN "100000" =>
																s_next_state <= I_OP;

										WHEN "010111" =>
																s_next_state <= Load_1;
										WHEN "010101" => 
																s_next_state <= Store;
										WHEN "000000" =>
																s_next_state <= Call;
										WHEN "000001" =>
																s_next_state <= Jump;
										WHEN "000110" =>	
																s_next_state <= Branch;
										WHEN "001110" =>	
																s_next_state <= Branch;
										WHEN "010110" =>	
																s_next_state <= Branch;
										WHEN "011110" =>	
																s_next_state <= Branch;
										WHEN "100110" =>	
																s_next_state <= Branch;
										WHEN "101110" =>	
																s_next_state <= Branch;
										WHEN "110110" =>	
																s_next_state <= Branch;
										WHEN "001100" =>	
																s_next_state <= UnsignedI;
										WHEN "010100" =>	
																s_next_state <= UnsignedI;
										WHEN "011100" =>	
																s_next_state <= UnsignedI;
										WHEN "101000" =>	
																s_next_state <= UnsignedI;
										WHEN "110000" =>	
																s_next_state <= UnsignedI;


										WHEN OTHERS => 
																s_next_state <= Fetch1;
									end Case;
				
				WHEN I_OP => 
									s_next_state <= Fetch1;
				
				WHEN R_OP =>
									s_next_state <= Fetch1;
				
				WHEN Load_1 => 
									s_next_state <= Load_2;
				
				WHEN Load_2 => 
									s_next_state <= Fetch1;
				
				WHEN Store =>
									s_next_state <= Fetch1;
				WHEN Branch => 
									s_next_state <= Fetch1;
				WHEN Call => 
									s_next_state <= Fetch1;
				WHEN Jump => 
									s_next_state <= Fetch1;
				When UnsignedI =>
									s_next_state <= Fetch1
				WHEN immR =>		
									s_next_state <= Fetch1;
				
				WHEN Break => 
									s_next_state <= Break;
							
				WHEN OTHERS =>
									s_next_state <= Fetch1;
			end CASE;

end process;


dff_state : process(clk, reset_n) is
begin
if reset_n = '0' then
	s_current_state <= Fetch1;
elsif rising_edge(clk) then
	s_current_state <= s_next_state;
end if;
end process;


output_generator : process(s_current_state) is
begin
			imm_signed <= '0';
			sel_b <= '0';
			sel_rC <= '0';
			sel_mem <= '0';
			sel_addr <= '0';
			read <= '0';
			write <= '0';
			ir_en <= '0';
			pc_en <= '0';
			rf_wren <= '0';
			-----------------
			pc_add_imm <= '0';
			pc_sel_a <= '0';
			pc_sel_imm <= '0';
			sel_pc <= '0';
			sel_ra <= '0';
			branch_op <= '0';
			------------------
			CASE s_current_state IS
				WHEN Fetch1 => 
									read <= '1';			
				WHEN Fetch2 => 
									ir_en <= '1';
									pc_en <= '1';
			
				WHEN I_OP => 
									rf_wren <= '1';
									imm_signed <= '1';
				
				WHEN R_OP =>
									rf_wren <= '1';
									sel_b <= '1';
									sel_rC <= '1';
				
				WHEN Load_1 => 
									imm_signed <= '1';
									sel_addr <= '1';
									read <= '1';
				
				WHEN Load_2 => 
									rf_wren <= '1';
									sel_mem <= '1';
				
				WHEN Store =>
									sel_addr <= '1';
									imm_signed <= '1';
									write <= '1';
				WHEN Branch =>				
									
									branch_op <= '1';
									pc_add_imm <= '1';
									sel_b <= '1';
				WHEN Call =>			 
									sel_pc <= '1';
									sel_ra <= '1';
									rf_wren <= '1';
									pc_sel_imm <= '1';
									pc_en = '1';

									if opx = "011101" then
										pc_sel_imm <= '1';
									else 	
										pc_sel_a = '1';
				WHEN Jump =>
										pc_en = '1';
									if op = "000001" then
                                                                                -- jmpi
										pc_sel_imm = '1';
									else
										pc_sel_a = '1';
				WHEN unsignedI =>
									rf_wren <= '1';
				WHEN immR =>
									rf_wren <= '1';
									sel_rC <= '1';


							
				WHEN OTHERS =>
									imm_signed <= '0';
			end CASE;
end process;

op_alu <= s_code_alu;

end synth;

										
--rf_wren <= s_rf_wren;
--sel_b <= s_sel_b;
--sel_rC <= s_sel_rC;
--sel_mem <= s_sel_mem;
--sel_addr <= s_sel_addr;
--read <= s_read;
--write <= s_write;
--ir_en <= s_ir_en;
--imm_signed <= s_signed;
--pc_en <= s_pc_en;
---------------------------
--pc_add_imm <= s_pc_add_imm;
--pc_sel_a <= s_pc_sel_a;
--pc_sel_imm <= s_pc_sel_imm;
--sel_pc <= s_sel_pc;
--sel_ra <= s_sel_ra;
--branch_op <= s_branch_op;
--SIGNAL s_signed : std_logic;
--SIGNAL s_sel_b : std_logic;
--SIGNAL s_sel_rC : std_logic;
--SIGNAL s_sel_mem : std_logic;
--SIGNAL s_sel_addr : std_logic;
--SIGNAL s_read : std_logic;
--SIGNAL s_write : std_logic;
--SIGNAL s_ir_en : std_logic;
--SIGNAL s_pc_en : std_logic;
--SIGNAL s_rf_wren : std_logic;

--SIGNAL s_pc_add_imm : std_logic;
--SIGNAL s_pc_sel_a : std_logic;
--SIGNAL s_pc_sel_imm : std_logic;
--SIGNAL s_sel_pc : std_logic;
--SIGNAL s_sel_ra : std_logic;
--SIGNAL s_branch_op : std_logic;


