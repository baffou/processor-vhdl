library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity add_sub is
    port(
        a        : in  std_logic_vector(31 downto 0);
        b        : in  std_logic_vector(31 downto 0);
        sub_mode : in  std_logic;
        carry    : out std_logic;
        zero     : out std_logic;
        r        : out std_logic_vector(31 downto 0)
    );
end add_sub;

architecture synth of add_sub is

CONSTANT c_full_vector : std_logic_vector(31 downto 0) := (others => '1');
CONSTANT c_empty_vector : std_logic_vector(31 downto 0) := (others => '0');
SIGNAL s_real_B : std_logic_vector(31 downto 0);
SIGNAL s_sum : std_logic_vector (31 DOWNTO 0);

SIGNAL s_unsigned_A : unsigned(32 downto 0);
SIGNAL s_unsigned_B : unsigned(32 downto 0);
SIGNAL s_sum_unsigned : unsigned(32 downto 0);

begin

s_real_B <= b XOR c_full_vector when sub_mode = '1' else b;

s_unsigned_A <= unsigned('0' & a);
s_unsigned_B <= unsigned('0' & s_real_B);
s_sum_unsigned <= s_unsigned_A + s_unsigned_B when sub_mode = '0' else
s_unsigned_A + s_unsigned_B + to_unsigned(1, 33);
s_sum <= std_logic_vector(s_sum_unsigned(31 downto 0));

carry <= s_sum_unsigned(32);
zero <= '1' when s_sum = c_empty_vector else '0';
r <= s_sum;
end synth;
