library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder is
    port(
        address : in  std_logic_vector(15 downto 0);
        cs_LEDS : out std_logic;
        cs_RAM  : out std_logic;
        cs_ROM  : out std_logic;
		  cs_buttons : out std_logic
    );
end decoder;

architecture synth of decoder is
signal s_leds : std_logic;
signal s_rom : std_logic;
signal s_ram : std_logic;
signal s_but : std_logic;

constant c_rom : integer := 16#1000#;
constant c_ram : integer := 16#2000#;
constant c_leds : integer := 16#2010#;
constant c_but_1 : integer := 16#2030#;
constant c_but_2 : integer := 16#2034#;

begin

decoding: process(address) is
begin
	s_leds <= '0';
	s_rom <= '0';
	s_ram <= '0';
	s_but <= '0';
	if to_integer(unsigned(address)) < c_rom then 
		s_rom <= '1';
	else
		if to_integer(unsigned(address)) < c_ram then  
			s_ram <= '1';
		else
			if to_integer(unsigned(address)) < c_leds then 
				s_leds <= '1';
			else
				if (to_integer(unsigned(address)) >= c_but_1) and (to_integer(unsigned(address)) <= c_but_2) then
					s_but <= '1';
				end if;
			end if;
		end if;
	end if;
end process;

cs_LEDS <= s_leds;
cs_ROM <= s_rom;
cs_RAM <= s_ram;
cs_buttons <= s_but;
end synth;
