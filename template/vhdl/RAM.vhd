library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RAM is
    port(
        clk     : in  std_logic;
        cs      : in  std_logic;
        read    : in  std_logic;
        write   : in  std_logic;
        address : in  std_logic_vector(9 downto 0);
        wrdata  : in  std_logic_vector(31 downto 0);
        rddata  : out std_logic_vector(31 downto 0));
end RAM;

architecture synth of RAM is

type reg_type is array(0 to 1023) of std_logic_vector(31 downto 0);

SIGNAL s_reg : reg_type;
SIGNAL s_enable_tri : std_logic;
SIGNAL s_mux_memory : std_logic_vector(9 downto 0);
SIGNAL s_memory_output : std_logic_vector(31 downto 0);
SIGNAL s_address : std_logic_vector(9 downto 0);

SIGNAL s_wrdata : std_logic_vector(31 downto 0);
SIGNAL s_w_address : std_logic_vector(9 downto 0);
SIGNAL s_write : std_logic;

begin

s_wrdata <= wrdata;
s_w_address <= address;
s_write <= write;

-- process pr register les data � �crire
register_data : process(clk,cs) is
begin
	if rising_edge(clk) then
		if (s_write and cs) = '1' then
			s_reg(to_integer(unsigned(s_w_address))) <= s_wrdata;
		end if;
	end if;
end process;





--dff pr l'adresse
dff_memory : process(address,clk) is 
begin
	if rising_edge(clk) then
		s_mux_memory <= address;
	end if;
end process;



--dff pr le tri-state buffer
dff_tri : process(read,cs,clk) is
begin
	if rising_edge(clk) then
		s_enable_tri <= read and cs;
	end if;
end process;




--input du tri-state buffer
s_memory_output <= s_reg(to_integer(unsigned(s_mux_memory)));
-- tri-state buffer
rddata <= s_memory_output when s_enable_tri = '1' else (others => 'Z');

end synth;
