library ieee;
use ieee.std_logic_1164.all;

entity ROM is
    port(
        clk     : in  std_logic;
        cs      : in  std_logic;
        read    : in  std_logic;
        address : in  std_logic_vector(9 downto 0);
        rddata  : out std_logic_vector(31 downto 0)
    );
end ROM;

architecture synth of ROM is

SIGNAL s_enable_tri : std_logic;
SIGNAL s_mux_memory : std_logic_vector(9 downto 0);
SIGNAL s_memory_output : std_logic_vector(31 downto 0);
SIGNAL s_address : std_logic_vector(9 downto 0);


component ROM_Block is PORT(
		address		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;

begin

rom_blockasse : ROM_Block port map( address => address,
				    clock => clk,
				    q => s_memory_output);

--dff pr le tri-state buffer
dff_tri : process(read,cs,clk) is
begin
	if rising_edge(clk) then
		s_enable_tri <= read and cs;
	end if;
end process;

rddata <= s_memory_output when s_enable_tri = '1' else (others => 'Z');
end synth;
