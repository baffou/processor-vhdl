library ieee;
use ieee.std_logic_1164.all;

entity extend is
    port(
        imm16  : in  std_logic_vector(15 downto 0);
        signed : in  std_logic;
        imm32  : out std_logic_vector(31 downto 0)
    );
end extend;

architecture synth of extend is

constant c_zero : std_logic_vector(15 downto 0) := "0000000000000000";
constant c_one : std_logic_vector(15 downto 0) := "1111111111111111";

signal s_with_zero : std_logic_vector(31 downto 0);
signal s_with_one : std_logic_vector(31 downto 0);

begin

s_with_zero <= c_zero & imm16;
s_with_one <= c_one & imm16;

imm32 <= s_with_one when ((imm16(15) = '1') and (signed = '1')) else s_with_zero;

end synth;
