library ieee;
use ieee.std_logic_1164.all;

entity comparator is
    port(
        a_31    : in  std_logic;
        b_31    : in  std_logic;
        diff_31 : in  std_logic;
        carry   : in  std_logic;
        zero    : in  std_logic;
        op      : in  std_logic_vector(2 downto 0);
        r       : out std_logic
    );
end comparator;


architecture synth of comparator is
SIGNAL s_output : std_logic;
begin

r <= NOT(carry) or zero when op = "101" else
     carry and NOT(zero) when op = "110" else
     (a_31 AND NOT(b_31)) OR ((a_31 XNOR b_31) AND (diff_31 OR zero)) when op = "001" else
     (NOT(a_31) AND b_31) OR ((a_31 XNOR b_31) AND (NOT(diff_31) AND NOT(zero))) when op = "010" else
     ('1' XOR zero) when op = "011" else
     zero;

   -- select_and_apply_op : process is
    --    begin 
     --       if op = "101" or op = "110" then
     --           if op = "101" then
     --               s_output <= NOT(carry) or zero;
      --          else
      --              s_output <= carry and NOT(zero);
     --           end if;
      --      else
      --          if op = "001" or op = "010" then
      --              if op = "001" then
      --                  s_output <= (a_31 AND NOT(b_31)) OR ((a_31 XNOR b_31) AND (diff_31 OR zero));
      --              else
      --                  s_output <= (NOT(a_31) AND b_31) OR ((a_31 XNOR b_31) AND (NOT(diff_31) AND NOT(zero)));
     --               end if;
     --           else
     --               if op = "011" then
     --                   s_output <= '1' XOR zero; 
     --               else
     --                   s_output <= zero;
    --                end if;
    --            end if;
   --         end if;
   -- end process;
  --  r <= s_output;
end synth;
