library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
    port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        en      : in  std_logic;
        sel_a   : in  std_logic;
        sel_imm : in  std_logic;
        add_imm : in  std_logic;
        imm     : in  std_logic_vector(15 downto 0);
        a       : in  std_logic_vector(15 downto 0);
        addr    : out std_logic_vector(31 downto 0)
    );
end PC;

architecture synth of PC is

signal s_actual_address : std_logic_vector(31 downto 0);
constant c_zero : std_logic_vector(15 downto 0) := "0000000000000000";

begin

dff : process(clk, reset_n, en) is

begin

if reset_n = '0' then
	s_actual_address <= c_zero & c_zero;

else 
	if rising_edge(clk) then
		if en = '1' then
			s_actual_address <= c_zero & std_logic_vector(to_unsigned(to_integer(unsigned(s_actual_address)) + 4, 16));
		end if;
	end if;
end if;

end process;

--attention à ce que les deux derniers bits soient bien 0

addr <= s_actual_address;

end synth;
